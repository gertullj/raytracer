perf record -F 99 -g ./raytracer
perf script | stackcollapse-perf.pl | flamegraph.pl --hash > raytracer-flamegraph.svg
firefox raytracer-flamegraph.svg
