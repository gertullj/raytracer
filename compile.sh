#!/usr/bin/bash
g++ -std=c++17 src/*.cpp -Ofast -ffast-math -Wall -Wextra -Iinclude -o raytracer -lpthread -march=native -DVERSION2 -DMODERN $@
