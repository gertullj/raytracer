CXX := g++-11
CC := gcc-11
CFLAGS   := -std=c17   -Wall -Wextra -Iinclude -march=native -Ofast -ffast-math -fno-math-errno -funroll-loops -finline-functions # -g -fno-omit-frame-pointer
CXXFLAGS := -std=c++17 -Wall -Wextra -Iinclude -march=native -Ofast -ffast-math -fno-math-errno -funroll-loops -finline-functions # -g -fno-omit-frame-pointer
LDFLAGS :=  -ldl -lpthread -flto -lSDL2 -lGL
DEFINES := -DVERSION2 -DKEEP_RUNNING -DGUI

BUILD_DIR := build
SRC_DIRS := src

SRCS := $(shell find $(SRC_DIRS) -name '*.cpp' -or -name '*.c')
OBJS := $(SRCS:$(SRC_DIRS)/%=$(BUILD_DIR)/%.o)

all: $(BUILD_DIR)/raytracer

build/%.c.o: src/%.c
	mkdir -p $(BUILD_DIR)
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@ $(DEFINES)

build/%.cpp.o: src/%.cpp
	mkdir -p $(BUILD_DIR)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@ $(DEFINES)

$(BUILD_DIR)/raytracer: $(OBJS)
	$(CXX) $(OBJS) $(LDFLAGS) -o $@

run:
	./build/raytracer $(ARGS)

.PHONY: clean

clean:
	rm -r $(BUILD_DIR)
