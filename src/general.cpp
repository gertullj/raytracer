#include <general.hpp>

// Color Color::operator+(const Color &rhs){
//     return Color(m_r + rhs.m_r, m_g + rhs.m_g, m_b + rhs.m_b);
// }
// 
// Color Color::operator*(const Color& rhs) {
//     return Color(m_r * rhs.m_r, m_g * rhs.m_g, m_b * rhs.m_b);
// }

// Color::Color() : m_r(0.0), m_g(0.0), m_b(0.0) {}
// Color::Color(float r, float g, float b) : m_r(r), m_g(g), m_b(b){}

// RGB888 Color::rgb888(){
//     return RGB888{.r = (uint8_t)(m_r * 255.0), .g = (uint8_t)(m_g * 255.0), .b = (uint8_t)(m_b * 255.0)};
// }


// const Color Black(0.0f, 0.0f, 0.0f);
// const Color White(1.0, 1.0, 1.0);


// Color Color::mix(const Color &rhs, float mix) {
//     return Color(
//         std::sqrt((1.0 - mix) * m_r * m_r + mix * rhs.m_r * rhs.m_r), 
//         std::sqrt((1.0 - mix) * m_g * m_g + mix * rhs.m_g * rhs.m_g),
//         std::sqrt((1.0 - mix) * m_b * m_b + mix * rhs.m_b * rhs.m_b)
//     );
// }

auto Color::mix(const Color *begin, const Color *end) -> Color {
    float rs = 0;
    float gs = 0;
    float bs = 0;
    for(int i = 0; i < end - begin; i++) {
        rs += begin[i].c_r * begin[i].c_r;
        gs += begin[i].c_g * begin[i].c_g;
        bs += begin[i].c_b * begin[i].c_b;
    }
    return Color(std::sqrt(rs / (float)(end - begin)), std::sqrt(gs / (float)(end - begin)), std::sqrt(bs / (float)(end - begin)));
}
