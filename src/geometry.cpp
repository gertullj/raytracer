#include <geometry.hpp>

Scene::Scene(): s_Spheres(), s_Planes(), s_Triangles(), s_Materials(), s_Skybox(){
    s_Materials.push_back(
        Material{
            .m_Color = Color(0, 0, 0), 
            .m_Reflect = 0.0, 
            .m_Diffuse = 0.0});
}


auto readOBJ(const char* filename, const float scale, const int default_material) -> std::vector<Triangle> {
    const auto before = std::chrono::high_resolution_clock::now();

    std::ifstream file(filename, std::ios::in);
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> tex_coords;
    std::vector<glm::vec3> normals;

    std::vector<int> vertex_indices;
    std::vector<int> texture_indices;
    std::vector<int> normal_indices;

    std::string line;
    std::string tok;
    std::string b1, b2, b3, b4;

    int num_faces = 0, num_triangles = 0;


    while(!file.eof()) {
        line = "";
        tok = "";
        // std::getline()
        std::getline(file, line);
        std::istringstream ss(line);

        if(line == "\n" || line[0] == '#' || isspace(line[0])) continue;
        ss >> tok;
        // std::equal()

        if(tok == "v") 
        { // vertices
            float v1, v2, v3;
            ss >> v1 >> v2 >> v3;

            vertices.push_back(glm::vec3(v1, v2, v3));
        } 
        else if(tok == "vn") 
        { // normals
            float n1, n2, n3;
            ss >> n1 >> n2 >> n3;
            normals.push_back(glm::vec3(n1, n2, n3));
        } 
        else if(tok == "vt") 
        { // texture coords
            float t1, t2, t3;
            ss >> t1 >> t2 >> t3;
            tex_coords.push_back(glm::vec3(t1, t2, t3));
            continue;
        }
        else if(tok == "f")
        { // faces: f v/t/n v/t/n v/t/n [v/t/n]       
            int i1, i2, i3, i4;
            int t1, t2, t3, t4;
            int n1, n2, n3, n4;     

            b4 = "";
            ss >> b1 >> b2 >> b3 >> b4;
            

            auto parse_triplet = [](std::string &tr, int &i, int &t, int &n) {
                auto slash1 = std::find(std::begin(tr), std::end(tr), '/');
                auto slash2 = std::find(slash1+1, std::end(tr), '/');
                // unschön, aber wir haben kein charconv...
                #ifdef MODERN
                std::from_chars(&*std::begin(tr), &*slash1, i);
                std::from_chars(&*(slash1+1), &*slash2, t);
                std::from_chars(&*(slash2+1), &*std::end(tr), n);
                #else
                i = std::stoi(std::string(std::begin(tr), slash1));
                t = std::stoi(std::string(slash1+1, slash2));
                n = std::stoi(std::string(slash2+1, std::end(tr)));
                #endif
            };

            parse_triplet(b1, i1, t1, n1);
            parse_triplet(b2, i2, t2, n2);
            parse_triplet(b3, i3, t3, n3);

            vertex_indices.push_back(i1);
            vertex_indices.push_back(i2);
            vertex_indices.push_back(i3);

            texture_indices.push_back(t1);
            texture_indices.push_back(t2);
            texture_indices.push_back(t3);

            normal_indices.push_back(n1);
            normal_indices.push_back(n2);
            normal_indices.push_back(n3);
            
            if(b4 != "") {
                parse_triplet(b4, i4, t4, n4);

                vertex_indices.push_back(i1);
                vertex_indices.push_back(i3);
                vertex_indices.push_back(i4);

                texture_indices.push_back(t1);
                texture_indices.push_back(t3);
                texture_indices.push_back(t4);        

                normal_indices.push_back(n1);
                normal_indices.push_back(n3);
                normal_indices.push_back(n4);

                num_faces++;
            } else {
                num_triangles++;
            }
        } else if (tok == "g") { // groups (comes before faces)
            continue;
        } else {
            std::cout << "could not parse line:\n" << line << "\n\n";
        }
    }
    std::cout
        << "Read: " << filename 
        << "\n\tVertices: " << vertices.size() 
        << "\n\tNormals: " << normals.size()
        << "\n\tTexcoords: " << tex_coords.size()
        << "\n\tFaces: " << num_faces 
        << "\n\tTriangles: " << num_triangles
        << "\n\t(" << vertex_indices.size() << " indices)\n\n";


    std::vector<Triangle> triangles;
    triangles.reserve(vertex_indices.size() / 3);


    for(size_t i = 0; i < vertex_indices.size() / 3; i++) {
        triangles.push_back(
            Triangle(
                vertices[vertex_indices[i*3  ]-1] * scale, 
                vertices[vertex_indices[i*3+1]-1] * scale, 
                vertices[vertex_indices[i*3+2]-1] * scale, 
                normals [normal_indices[i*3  ]-1] * scale,
                normals [normal_indices[i*3+1]-1] * scale,
                normals [normal_indices[i*3+2]-1] * scale,
                default_material));
    }

    const auto after = std::chrono::high_resolution_clock::now();
    std::cout << "OBJ file " 
        << filename  << " loaded " 
        << num_faces << " faces and " 
        << num_triangles << " triangles; took " 
        << std::chrono::duration_cast<std::chrono::milliseconds>(after - before).count() << "ms\n";

    return triangles;
}