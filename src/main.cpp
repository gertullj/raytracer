#include <general.hpp>
#include <draw.hpp>
#include <geometry.hpp>
#include <boost/lockfree/queue.hpp>

// #define MODERN
#ifdef MODERN
#include <charconv>
#include <string_view>
#endif


static thread_local std::random_device rd;
static thread_local std::mt19937 defrand(rd());

const char *const NEWLINE = "\n"; 


// questionable!
auto subsphereRand(const float phi0, const float phi1, const float theta0, const float theta1) -> glm::vec3 {
    const float phi = std::uniform_real_distribution(phi0, phi1)(defrand);
    const float theta = std::uniform_real_distribution(theta0, theta1)(defrand);
    const float sin_theta = std::sin(theta);
    const float x = std::cos(phi) * sin_theta;
    const float y = std::sin(phi) * sin_theta;
    const float z = std::cos(theta);
    return glm::vec3(x, y, z);
}

inline auto sphereRand() -> glm::vec3 {
#ifdef VERSION1
    //VERSION 1
    glm::vec3 ball = glm::ballRand(1.0f);
    return ball;
#elif defined(VERSION2)
    // VERSION 2
    glm::vec3 ball;
    static thread_local auto dist = std::uniform_real_distribution(-1.0f, 1.0f);
    do {
        ball.x = dist(defrand);
        ball.y = dist(defrand);
        ball.z = dist(defrand);
    } while (glm::dot(ball, ball) > 1.0);
    return ball;

#else
    // VERSION 3
    glm::vec3 ball;
    static thread_local auto full_circle = std::uniform_real_distribution(0.0f, M_2_PIf32);
    static thread_local auto zero_one = std::uniform_real_distribution(0.0f, 1.0f);
    const float theta = full_circle(defrand);
    const float v = zero_one(defrand);
    const float phi = std::acos(2.0 * v - 1.0);
    const float r = std::cbrt(zero_one(defrand)); // std::pow(zero_one(defrand), 0.33333333333333);
    ball.x = r * std::sin(phi) * std::cos(theta);
    ball.y = r * std::sin(phi) * std::sin(theta);
    ball.z = r * std::cos(phi);
    return ball;
#endif
}


auto hemiRand(const glm::vec3& normal) -> glm::vec3 {
    // viel langsamer, wird zum Bottleneck O(30) Sekunden
    // auto ball = glm::ballRand<float>(1.0);

    // Resultat ununterscheidbar O(6) Sekunden
    glm::vec3 ball;
    static thread_local auto dist = std::uniform_real_distribution(-1.0f, 1.0f);

    do{
        ball.x = dist(defrand);
        ball.y = dist(defrand);
        ball.z = dist(defrand);
    } while(false);//glm::dot(ball, ball) > 1.0);


    if(glm::dot(ball, normal) < 0.0f){
        return -ball;
    } else {
        return ball;
    }
    
}


typedef std::tuple<MatID, float, glm::vec3> HitRecord;
constexpr HitRecord NO_HIT = std::make_tuple(0, std::numeric_limits<float>::infinity(), glm::vec3(0, 0, 0));

constexpr auto intersectSphere(const Sphere& sphere, const Ray& ray) -> HitRecord {
    const glm::vec3 diff = ray.r_Origin - sphere.s_Position;
    const float a = glm::dot(ray.r_Direction, ray.r_Direction);
    const float half_b = glm::dot(ray.r_Direction, diff);
    const float c = glm::dot(diff, diff) - sphere.s_Radius*sphere.s_Radius;
    const float dis = half_b * half_b - a * c;
    if (dis < 0) {
        //miss
        return NO_HIT;
    }
    // std::cout << "Intersection\n";

    const float t = ((-half_b) - std::sqrt(dis)) / a;
    if(t < 0) {
        return NO_HIT;
    }
    
    return std::make_tuple(sphere.s_Mat, t, glm::normalize(ray.at(t) - sphere.s_Position));
}

constexpr auto intersectPlane(const Plane& plane, const Ray& ray) -> HitRecord {
    const float distance = glm::dot(plane.p_Point - ray.r_Origin, plane.p_Normal)/glm::dot(ray.r_Direction, plane.p_Normal);
    if(distance < 0) {
        return NO_HIT;
    }
    return std::make_tuple(plane.p_Mat, distance, plane.p_Normal);
}

constexpr auto intersectTriangle(const Triangle& tri, const Ray& ray) -> HitRecord {
    constexpr auto EPS = 0.00001f;
    
    const glm::vec3 edge1 = tri.t_V1 - tri.t_V0;
    const glm::vec3 edge2 = tri.t_V2 - tri.t_V0;


    const glm::vec3 h = glm::cross(ray.r_Direction, edge2);
    const float a = glm::dot(h, edge1);

    if(a > -EPS && a < EPS) return NO_HIT;

    const float f = 1.0f / a;
    const glm::vec3 s = ray.r_Origin - tri.t_V0;
    const float u = f * glm::dot(s, h);

    if(u < 0.0f || u > 1.0f) return NO_HIT;

    const glm::vec3 q = glm::cross(s, edge1);
    const float v = f * glm::dot(ray.r_Direction, q);

    if(v < 0.0f || u + v > 1.0f) return NO_HIT;

    const float t = f * glm::dot(edge2, q);
    if(t > EPS) {
        // auto intersection = ray.at(t);
        // return std::make_tuple(tri.mat, t, glm::cross(edge1, edge2));
        return std::make_tuple(tri.t_Mat, t, tri.t_N1 * u + tri.t_N2 * v + tri.t_N0 * (1.0f - u - v)); // probably wrong...
    } else return NO_HIT;


    // auto normal = glm::cross(tri.v1 - tri.v0, tri.v2 - tri.v0);
    // float area2 = glm::length(normal);
    // float n_dot_dir = glm::dot(normal, ray.direction);
    // const std::tuple<Material, float, glm::vec3> noHit = std::make_tuple(NaM, std::numeric_limits<float>::infinity(), glm::vec3(0.0, 0.0, 0.0));
// 
    // // parallel
    // if(std::abs(n_dot_dir) < 0.001) return noHit; 
// 
    // float d = glm::dot(normal, tri.v0);
    // float t = (glm::dot(normal, ray.origin) + d) / n_dot_dir;
// 
    // // ray behind triangle
    // if(t < 0) return noHit;
// 
    // auto intersection = ray.at(t);
    // auto C = glm::cross(tri.v1 - tri.v0, intersection - tri.v0);
    // if(glm::dot(C, normal) < 0.0) return noHit;
    // C = glm::cross(tri.v2 - tri.v1, intersection - tri.v1);
    // if(glm::dot(C, normal) < 0.0) return noHit;
    // C = glm::cross(tri.v0 - tri.v2, intersection - tri.v2);
    // if(glm::dot(C, normal) < 0.0) return noHit;
// 
    // // auto c = std::min(
    // //     std::min(
    // //         glm::dot(, normal),
    // //         glm::dot(, normal)
    // //     ),
    // //     glm::dot(, normal)
    // // );
    // // if(c < 0) return snoHit;
// 
    // return std::make_tuple(tri.mat, t, normal);
}

auto castRay(const Scene& scene, const Ray& ray, const int bounces) -> Color {
    // auto background = Color(0.8, 0.8, 0.8);
    HitRecord closest = NO_HIT;

    for(const Sphere& s : scene.s_Spheres) {
        auto result = intersectSphere(s, ray);
        if(std::abs(std::get<1>(result)) < std::get<1>(closest)) {
           closest = result;
        }
    }

    for(const Plane& p : scene.s_Planes) {
        auto result = intersectPlane(p, ray);
        if(std::abs(std::get<1>(result)) < std::get<1>(closest)) {
            closest = result;
        }
    }

    for(const Triangle& t : scene.s_Triangles) {
        auto result = intersectTriangle(t, ray);
        if(std::abs(std::get<1>(result)) < std::get<1>(closest)) {
            closest = result;
        }
    }

    const auto hitMat = std::get<0>(closest);
    const auto hitDist = std::get<1>(closest);
    const auto hitNormal = std::get<2>(closest);
    const auto hitColor = scene.s_Materials[hitMat].m_Color;
    
    
    // skybox intersect
    if(hitDist == std::numeric_limits<float>::infinity()) {
        const glm::vec3 dir = ray.r_Direction;
        const glm::vec3 absdir = glm::abs(dir);
        float ma;
        float u, v;
        int face_idx = 0;
        if(absdir.z >= absdir.x && absdir.z >= absdir.y) {
            face_idx = dir.z < 0.0 ? 5 : 4;
            ma = 0.5 / absdir.z;
            u = dir.z < 0.0 ? -dir.x : dir.x;
            v = -dir.y;
        }
        else if(absdir.y >= absdir.x) {
            face_idx = dir.y < 0.0 ? 3 : 2;
            ma = 0.5 / absdir.y;
            u = dir.x;
            v = dir.y < 0.0 ? -dir.z : dir.z;
        }
        else {
            face_idx = dir.x < 0.0 ? 1 : 0;
            ma = 0.5 / absdir.x;
            u = dir.x < 0.0 ? dir.z : -dir.z;
            v =  -dir.y;
        }
        u = u * ma + 0.5;
        v = v * ma + 0.5;

        const int width_div = scene.s_Skybox.t_Width / 4;
        const int height_div = scene.s_Skybox.t_Height / 3;
        int i, j;
        switch(face_idx) {

            case 4: { // (1, 0) UP
                i = width_div * 2 - (float)width_div * v;
                j = height_div * 0 + (float)height_div * u;
            } break;
            case 5: { // (1, 2) DOWN
                i = width_div * 2 - (float)width_div * v;
                j = height_div * 2 + (float)height_div * u;
            } break;

            case 2: { // (2, 1) RIGHT
                i = width_div * 3 - (float)width_div * u;
                j = height_div * 2 - (float)height_div * v;
            } break;
            case 3: { // (0, 1) LEFT
                i = width_div * 0 + (float)width_div * u;
                j = height_div * 1 + (float)height_div * v;
            }break;
            case 0: { // (1, 1) FRONT
                i = width_div * 2 - (float)width_div * v;
                j = height_div * 1 + (float)height_div * u;
            } break;
            case 1: { // (3, 1) BACK
                i = width_div * 3 + (float)width_div * v;
                j = height_div * 2 - (float)height_div * u;
            } break;
            
            //default:
            //    bg = skybox.at(i, j);
        }


        return scene.s_Skybox.at(i, j) * 1.0f;
    }

    
    if (bounces > 0)
    {
        const glm::vec3 normal = glm::normalize(hitNormal);
        const glm::vec3 intersection = ray.at(hitDist - 0.001);
        const glm::vec3 reflected = glm::reflect(ray.r_Direction, normal);
        const auto material = scene.s_Materials[hitMat];

        Color cols[1]; // samples per hit
        for(auto &c : cols) {
            const auto newRay = Ray(
                intersection, 
                glm::normalize(reflected * material.m_Reflect + (normal + sphereRand()) * material.m_Diffuse));
            // auto newRay = Ray(intersection, normal + sphereRand());
            c = castRay(scene, newRay, bounces-1);
            // c = cast;
        }
        const auto mixed = Color::mix(cols, cols + sizeof(cols)/sizeof(cols[0]));
        return hitColor * mixed;
    }
    else // ambient/max recursion color 
    {
        return Color(0.1, 0.1, 0.1);//Black;//hitColor * background;
    }
}


auto render(Img &img, const Scene &scene, const int SPP, const int BOUNCES, int x0, int x1, int y0, int y1) {
    constexpr float df = 0.002; // sampling unsharpness
    static thread_local auto dist = std::uniform_real_distribution(-df, df);
    constexpr float rect_x = 1.0;
    constexpr float rect_y = 1.0;


    const glm::vec3 cameraPos(-5.0, -5.0, 3.0);
    const glm::vec3 cameraDir = glm::normalize(glm::vec3(0.0, 0.0, 0.5) - cameraPos);
    const glm::vec3 cameraUp(0.0, 0.0, 1.0);
    const glm::vec3 cameraRight = glm::normalize(glm::cross(cameraUp, cameraDir));

    #ifdef CONT // run continuously
    while(true){
            int i = std::uniform_int_distribution<int>(x0, x1-1)(defrand);
            int j = std::uniform_int_distribution<int>(y0, y1-1)(defrand);
    
    #else // run once for every pixel
    
    for(int j = y0; j < y1; j++) {
        for(int i = x0; i < x1; i++) {

    #endif
            /// square canvas ///
            float dx = -(rect_x * 0.5) + (float)i * rect_x / (float) img.i_Width;
            float dy = -(rect_y * 0.5) + (float)j * rect_y / (float) img.i_Height;
            dx *= (float)img.i_Width/(float)img.i_Height;
            auto dir = glm::normalize(cameraDir + dx * cameraRight + dy * cameraUp);
            
            /// spherical canvas ///  
            // float theta = M_PI * (1.0 - ((float)j * 1.0/(float)img.m_Height));
            // float phi = 8 * M_2_PI * (-0.5 + i * 1.0 / (float)img.m_Width);
            // auto dir = glm::vec3(
            //     std::sin(theta) * std::cos(phi),
            //     std::sin(theta) * std::sin(phi),
            //     std::cos(theta)
            // );


            Ray r(cameraPos, dir);

            Color cols[SPP]; // samples per pixel
            for(auto &c : cols) {
                //glm::vec3 df = glm::vec3(0.0, std::uniform_real_distribution<float>(-0.05, 0.05)(defrand), std::uniform_real_distribution<float>(-0.05, 0.05)(defrand));
                //r.origin = cameraPos + df;
                const glm::vec3 delta(0.0, dist(defrand), dist(defrand));
                r.r_Direction = glm::normalize(dir + delta);//subsphereRand(-df, df, M_PI_2-dw, M_PI_2+dw));
                // c = castRay(scene, r, 4);
                c = castRay(scene, r, BOUNCES);
            }
            img.at(i, j) = Color::mix(cols, cols + sizeof(cols)/sizeof(cols[0])).rgb888(); // mix(mix(c1, c2, 0.5), mix(c3, c4, 0.5), 0.5);
    #ifndef CONT
        }
    #endif
    }
}


auto generate_random_spheres(const unsigned int n, const unsigned int num_materials, std::vector<Sphere> &out) {
    auto dist_x = std::uniform_real_distribution(-100.0f, 100.0f);
    auto dist_y = std::uniform_real_distribution(-100.0f, 100.0f);
    auto dist_z = std::uniform_real_distribution(1.0f, 2.0f);
    auto dist_radius = std::uniform_real_distribution(0.0f, 1.0f);
    for(unsigned int i = 0; i < n; i++){
        out.push_back(Sphere{
            .s_Position = glm::vec3(
                dist_x(defrand),
                dist_y(defrand),
                dist_z(defrand)
            ),
            .s_Radius = dist_radius(defrand),
            .s_Mat = (i % (num_materials - 1)) + 1 // use any material except the debug material 0
        });
    }
}

struct WorkElement{int x0, x1, y0, y1;};

auto usage() {
    std::cout <<
        "Usage: raytracer [-size <WIDTH>x<HEIGHT>] [-bounces <BOUNCES>] [-spp <SAMPLES_PER_PIXEL] [outfile] \n";
}

auto main(int argc, char **argv) -> int {
    std::cout << "Hello World" << NEWLINE;

    int WIDTH = 800;
    int HEIGHT = 600;
    int SPP = 10;
    int BOUNCES = 4;

    std::string outfile{};

    // argument parsing
    if(argc > 1) {
        for(int i = 1; i < argc; i++) {
            if(strcmp(argv[i], "-size") == 0) { // raytracer ... -size <WIDTH>x<HEIGHT>
                if(++i < argc) { // advance i before comparison
                    auto arg_len = strlen(argv[i]);
                    auto div =  std::find(argv[i], argv[i]+arg_len, 'x');
                    *div = '\0';
                    WIDTH = atoi(argv[i]);
                    HEIGHT = atoi(div+1);
                } else {
                    usage();
                    return EXIT_FAILURE;
                }
            } else if(strcmp(argv[i], "-bounces") == 0) { // raytracer ... -bounces <BOUNCES>
                if(++i < argc) { // advance i before comparison
                    const auto arg_len = strlen(argv[i]);
                    BOUNCES = atoi(argv[i]);
                } else {
                    usage();
                    return EXIT_FAILURE;
                }
            } else if(strcmp(argv[i], "-spp") == 0) { // raytracer ... -spp <SAMPLES_PER_PIXEL>
                if(++i < argc) { // advance i before comparison
                    const auto arg_len = strlen(argv[i]);
                    SPP = atoi(argv[i]);
                } else {
                    usage();
                    return EXIT_FAILURE;
                }
            } else if(strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "-help") == 0) {
                usage();
                return EXIT_SUCCESS;
            } 
            else {
                outfile = argv[i]; // output file
            }
        }
    }

    std::cout << "*** Running raytracer ***\n" 
        << "\tFrame size: " << WIDTH << "x" << HEIGHT << "\n"
        << "\tBounces: " << BOUNCES << "\n"
        << "\tSamples per pixel: " << SPP << "\n\n";

    Texture tex("assets/stormydays_large.jpg");
    tex.t_Fallback = Color(1.0, 1.0, 1.0);



    #ifdef GUI
    Context context("Raytracer", WIDTH, HEIGHT);
    auto &img = context.c_Img;
    #else
    auto img = Img(WIDTH, HEIGHT);
    #endif
	std::cout << "Done generating Context" << std::endl;

    auto teapot = readOBJ("assets/teapot.obj", 0.1f, 1);

    Scene scene;
    scene.s_Skybox = std::move(tex);

    const int num_materials = 100;

    scene.s_Materials.push_back(Material{.m_Color = Color(0.9f, 0.9f, 0.9f), .m_Reflect = 1.0f, .m_Diffuse = 0.0f}); // material 1
    scene.s_Materials.push_back(Material{.m_Color = Color(0.2f, 0.8f, 0.8f), .m_Reflect = 1.0f, .m_Diffuse = 0.0f});
    scene.s_Materials.push_back(Material{.m_Color = Color(0.8f, 0.6f, 0.2f), .m_Reflect = 1.0f, .m_Diffuse = 0.0f});
    scene.s_Materials.push_back(Material{.m_Color = Color(0.8f, 0.2f, 0.3f), .m_Reflect = 1.0f, .m_Diffuse = 0.0f});
    scene.s_Materials.push_back(Material{.m_Color = Color(0.5f, 0.8f, 0.0f), .m_Reflect = 0.0f, .m_Diffuse = 1.0f});
    scene.s_Materials.push_back(Material{.m_Color = Color(0.8f, 0.8f, 0.8f), .m_Reflect = 0.9f, .m_Diffuse = 0.1f});

    auto zero_to_one = std::uniform_real_distribution(0.0f, 1.0f);
    const int prev_size = scene.s_Materials.size();
    for(int i = 0; i < num_materials - prev_size + 1; i++){
        float reflect = zero_to_one(defrand);
        scene.s_Materials.push_back(Material{
            .m_Color = Color(
                zero_to_one(defrand),
                zero_to_one(defrand),
                zero_to_one(defrand)),
            .m_Reflect = reflect,
            .m_Diffuse = 1.0f - reflect});
    }

    // sphere triplet at origin
    // scene.s_Spheres.push_back(Sphere{.s_Position = glm::vec3(1.0, 0.0, 0.2), .s_Radius = 0.5, .s_Mat = 1});
    // scene.s_Spheres.push_back(Sphere{.s_Position = glm::vec3(1.0, -1.0, 0.2), .s_Radius = 0.5, .s_Mat = 2});
    // scene.s_Spheres.push_back(Sphere{.s_Position = glm::vec3(1.0, 1.0, 0.2), .s_Radius = 0.5, .s_Mat = 3});

    generate_random_spheres(2000, num_materials, scene.s_Spheres);

    // ground plane
    scene.s_Planes.push_back(Plane{.p_Point = glm::vec3(0.0, 0.0, -0.3), .p_Normal = glm::vec3(0.0 ,0.0, 1.0), .p_Mat = 6});

    // teapot
    scene.s_Triangles.insert(scene.s_Triangles.end(), teapot.begin(), teapot.end());


    std::vector<std::thread> threads;

    const int NUM_THREADS = std::thread::hardware_concurrency(); // get number of available cores

    std::atomic<bool> handles[NUM_THREADS];
    for(auto &h : handles) {
        h = false;
    }

    boost::lockfree::queue<WorkElement, boost::lockfree::capacity<1000>> work_queue;

    int num_chunks = 0;
    std::atomic<int> done_chunks = 0;

    for(int j = 0; j < img.i_Height; j+=80){
        for(int i = 0; i < img.i_Width; i+=100){
            work_queue.push(
                WorkElement{
                    .x0 = i, 
                    .x1 = std::min((int) img.i_Width, i + 100), 
                    .y0 = j, 
                    .y1 = std::min((int)img.i_Height, j + 80)});
            num_chunks++;
        }
    }
    std::cout << "Spawning " << NUM_THREADS << " threads\n";
    for(int i = 0; i < NUM_THREADS; i++){
        threads.push_back(std::thread([=, &img, &scene, &handles, &work_queue, &done_chunks](){
            WorkElement region;
            while(work_queue.pop(region)) {
                // std::cout << "Thread " << i << " now working on region (" << region.x0 << "," << region.x1 << ";" << region.y0 << "," << region.y1 << ")" << std::endl;
                
                #ifdef GUI
                for(int m = region.x0; m < region.x1; m++){
                    img.at(m, region.y0) = RGB888{.r = 100, .g = 100, .b = 100};
                    img.at(m, region.y1-1) = RGB888{.r = 100, .g = 100, .b = 100};
                }
                for(int n = region.y0; n < region.y1; n++){
                    img.at(region.x0, n) = RGB888{.r = 100, .g = 100, .b = 100};
                    img.at(region.x1-1, n) = RGB888{.r = 100, .g = 100, .b = 100};
                    
                }
                #endif

                // auto t1 = std::chrono::high_resolution_clock::now();
                
                render(img, scene, SPP, BOUNCES, region.x0, region.x1, region.y0, region.y1);
                // auto t2 = std::chrono::high_resolution_clock::now();
                // std::cout << "Frame rendered in " << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count() << "\n";
                // std::cout << "Thread " << i << " done." << std::endl;
                done_chunks++;
            }
            handles[i] = true;
        }));
    }


    // float dt = 0.0;
    auto tstart = std::chrono::high_resolution_clock::now();
    auto t1 = std::chrono::high_resolution_clock::now();
    int done = 0;
    #ifdef GUI
    while(context.update()) {
    #else
    while(true) {
    #endif
        auto t2 = std::chrono::high_resolution_clock::now();
        auto diff = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1);
        auto sleep_time = std::chrono::milliseconds(100) - diff;
        // std::cout << "Thread " << std::this_thread::get_id() << ": This thread will sleep for " << sleep_time.count() << "ms\n";
        std::this_thread::sleep_for(sleep_time);
        t1 = t2;

        // progress bar
        float percent = (float)done_chunks/(float)num_chunks * 100.0f;
        std::cout << "\r\x1b[J[" // go to beginning of line and clear it
            << std::setfill('*') << std::setw((int)(std::ceil(percent/2.0f))) << "" 
            << std::setfill(' ') << std::setw(50 - (int)(std::ceil(percent/2.0f))) << "]" 
            << " (" << std::fixed << std::setprecision(1) << std::setw(5) << percent << "%)" << std::flush;

        // check if threads have finished
        for(int i = done; i < NUM_THREADS; i++) {
            if(handles[i] == false) { // first unfinished thread
                goto cont;
            } else {
                done++;
            }
        }
        goto finish;
        cont:;

        

    } {
        for(auto &th : threads) th.~thread(); // kill threads prematurely
    }
    finish:;
    for(auto &th : threads) th.join(); // join all threads


    auto tend = std::chrono::high_resolution_clock::now();
    uint64_t millis = (int64_t) std::chrono::duration_cast<std::chrono::milliseconds>(tend - tstart).count();
    uint64_t days = millis / (24 * 60 * 60 * 1000);
    millis = millis % (24 * 60 * 60 * 1000);
    uint64_t hours = millis / (60 * 60 * 1000);
    millis = millis % (60 * 60 * 1000);
    uint64_t minutes = millis / (60 * 1000);
    millis = millis % (60 * 1000);
    uint64_t seconds = millis / 1000;
    millis = millis % 1000;  

    std::cout << "\nImage rendered in "
        // << std::setfill('0') << std::setw(2) 
        << days << "d" // << std::setfill('0') << std::setw(2) 
        << hours << "h" // << std::setfill('0') << std::setw(2) 
        << minutes << "m" // << std::setfill('0') << std::setw(2) 
        << seconds << "." << std::setfill('0') << std::setw(3) 
        << millis << "s\n";

    #if defined(GUI) && defined(KEEP_RUNNING)
    while(context.update()){;;}
    #endif

    // save:
    if(outfile == "") {
        time_t now = time(0);
        tm *ltm = localtime(&now);
        std::ostringstream of;

        of  << "out/" 
            << 1900 + ltm->tm_year
            << "-" << std::setw(2) << std::setfill('0') << 1 + ltm->tm_mon
            << "-" << std::setw(2) << std::setfill('0') << ltm->tm_mday
            << "-" << std::setw(2) << std::setfill('0') << ltm->tm_hour
            << ":" << std::setw(2) << std::setfill('0') << ltm->tm_min
            << ":" << std::setw(2) << std::setfill('0') << ltm->tm_sec
            << ".ppm" << std::flush;

        std::cout << "Writing image to " << of.str() << std::endl;
        img.savePPM(of.str());
    } else {
        img.savePPM(outfile);
    }

    return 0;
}
