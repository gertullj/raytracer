#include <draw.hpp>
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#include <fstream>

#include <cassert>


#ifdef GUI
void APIENTRY gl_callback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam){
    printf("GL: %d: %d %u %d (%d) %p: %s\n", type, source, id, severity, length, userParam, message);
}


const char *vertex_shader = \
"#version 440 core\n" \
"layout(location=0) in vec2 pos;\n"\
"layout(location=1) in vec2 tex;\n"\
"layout(location=0) out vec2 oTex;\n"\
"void main(){\n"\
"gl_Position=vec4(pos.xy, 0.0, 1.0);\n"\
"oTex = tex;\n"\
"}\n";

const char *fragment_shader = \
"#version 440 core\n" \
"layout(location=0) in vec2 oTex;\n"\
"out vec4 FragColor\n;"\
"uniform sampler2D image;\n"\
"void main(){\n"\
"FragColor = texture(image, oTex);\n"\
"}\n";
#endif

Img::Img(const size_t width, const size_t height) : i_Width(width), i_Height(height) {
    i_Data = new RGB888[i_Width * i_Height]; // (RGB888*) calloc(width * height, sizeof(RGB888));
}

Img::Img(const Img& other) : i_Width(other.i_Width), i_Height(other.i_Height) {
    i_Data = new RGB888[i_Width * i_Height];
    memcpy(i_Data, other.i_Data, i_Width * i_Height * sizeof(RGB888));
}

Img::Img(Img&& other) : i_Width(other.i_Width), i_Height(other.i_Height), i_Data(other.i_Data) {
    other.i_Data = nullptr;
}

void Img::operator=(const Img& other) {
    i_Width = other.i_Width;
    i_Height = other.i_Height;
    if(i_Data)
        delete[] i_Data;
    
    if(other.i_Data) {
        i_Data = new RGB888[i_Width * i_Height];
        memcpy(i_Data, other.i_Data, i_Width * i_Height * sizeof(RGB888));
    } else {
        i_Data = nullptr;
    }
}

void Img::operator=(Img&& other) {
    i_Width = other.i_Width;
    i_Height = other.i_Height;
    if(i_Data) delete [] i_Data;
    i_Data = other.i_Data;
    other.i_Data = nullptr;
}

Img::~Img() {
    if(i_Data)
        delete[] i_Data;        
}

RGB888& Img::at(const size_t i, const size_t j) {
    return i_Data[j * i_Width + i];
}

void Img::write(std::ostream& stream) {
    for(unsigned int i = 1; i <= i_Height; i++) {
        stream.write((const char*) i_Data + (i_Height - i) * i_Width * 3, i_Width * 3);
    }
}

void Img::savePPM(const std::string& filename) {
    std::fstream file(filename, std::ios::out);
    file << "P6\n# " << filename << "\n" << i_Width << " " << i_Height << "\n255\n";
    write(file);
}

void Img::savePPM(const char *filename) {
    std::fstream file(filename, std::ios::out);
    file << "P6\n# " << filename << "\n" << i_Width << " " << i_Height << "\n255\n";
    write(file);
}

#ifdef GUI
Context::Context(const char *title, int width, int height){
    SDL_version version;
    SDL_GetVersion(&version);
    std::cout << "SDL Version " << (int) version.major << "." << (int) version.minor << "." << (int) version.patch << std::endl;

    if(SDL_Init(SDL_INIT_VIDEO) < 0){
		std::cout << "Error initializing SDL " << SDL_GetError() << std::endl;
	}
    
    if(width == 0 || height == 0) {
        SDL_Rect display_bounds;
        SDL_GetDisplayUsableBounds(0, &display_bounds);
        width = width == 0 ? display_bounds.w : width;
        height = height == 0 ? display_bounds.h : height;
    }
    
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 4);
    
    
    c_Win = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_OPENGL);
    SDL_ShowWindow(c_Win);
    
    int draw_width = width, draw_height = height;
    SDL_GL_GetDrawableSize(c_Win, &draw_width, &draw_height);

    // int top, left, bottom, right;
    // SDL_GetWindowBordersSize(c_Win, &top, &left, &bottom, &right);
    // 
    // draw_width = draw_width - left - right;
    // draw_height = draw_height - top - bottom;

    c_Img = Img(draw_width, draw_height);



    c_GL_ctx = SDL_GL_CreateContext(c_Win);
    SDL_GL_MakeCurrent(c_Win, c_GL_ctx);
	std::cout << "Created GL Context" << std::endl;
    
    gladLoadGLLoader(SDL_GL_GetProcAddress);

	int major, minor;
	glGetIntegerv(GL_MAJOR_VERSION, &major);
	glGetIntegerv(GL_MINOR_VERSION, &minor);
	
	std::cout << "GL Version " << major << "." << minor << "\n";

    glEnable(GL_DEBUG_OUTPUT);
    glDebugMessageCallback(gl_callback, NULL);


    glClearColor(1.0, 0.0, 0.0, 1.0);

	std::cout << "GL functions work!" << std::endl;

    static const float quad[] = {
        -1.0, -1.0, 0.0, 0.0, // ul  
        1.0, 1.0, 1.0, 1.0, // or
        -1.0, 1.0, 0.0, 1.0, // ol
        -1.0, -1.0, 0.0 ,0.0, // ul
        1.0, -1.0, 1.0, 0.0, // ur
        1.0, 1.0, 1.0, 1.0, // or
    };

    glGenVertexArrays(1, &c_VAO);
    glBindVertexArray(c_VAO);
    glGenBuffers(1, &c_VBO);
    glBindBuffer(GL_ARRAY_BUFFER, c_VBO);

    glBufferData(GL_ARRAY_BUFFER, sizeof(quad), quad, GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(quad[0]), (void*) 0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(quad[0]), (void*) (2 * sizeof(quad[0])));
    
    glBindVertexArray(0);

    GLuint vert = glCreateShader(GL_VERTEX_SHADER);
    GLuint frag = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(vert, 1, &vertex_shader, NULL);
    glShaderSource(frag, 1, &fragment_shader, NULL);
    glCompileShader(vert);
    glCompileShader(frag);

    GLint success = GL_FALSE;
    glGetShaderiv(vert, GL_COMPILE_STATUS, &success);
    if(success != GL_TRUE) {
        GLint log_length;
        glGetShaderiv(vert, GL_INFO_LOG_LENGTH, &log_length);
        char log[log_length];
        glGetShaderInfoLog(vert, log_length, &log_length, log);
        printf("Error compiling vertex shader:\n%s\n", log);
    }

    success = GL_FALSE;
    glGetShaderiv(frag, GL_COMPILE_STATUS, &success);
    if(success != GL_TRUE) {
        GLint log_length;
        glGetShaderiv(frag, GL_INFO_LOG_LENGTH, &log_length);
        char log[log_length];
        glGetShaderInfoLog(frag, log_length, &log_length, log);
        printf("Error compiling fragment shader:\n%s\n", log);
    }


    c_Prog = glCreateProgram();
    glAttachShader(c_Prog, vert);
    glAttachShader(c_Prog, frag);
    glLinkProgram(c_Prog);

    success = GL_FALSE;
    glGetProgramiv(c_Prog, GL_LINK_STATUS, &success);
    if(success != GL_TRUE) {
        GLint log_length;
        glGetProgramiv(c_Prog, GL_INFO_LOG_LENGTH, &log_length);
        char log[log_length];
        glGetProgramInfoLog(c_Prog, log_length, &log_length, log);
        printf("Error linking program:\n%s\n", log);
    }

    // uint8_t *image = (uint8_t*) calloc(WIDTH * HEIGHT * 3, sizeof(uint8_t));
    

    // for(int i = 50000; i < 100000; i+=3) image[i] = 255;

    glGenTextures(1, &c_Tex);
    glBindTexture(GL_TEXTURE_2D, c_Tex);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, c_Img.i_Width, c_Img.i_Height, 0, GL_RGB, GL_UNSIGNED_BYTE, c_Img.i_Data);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

}

bool Context::update() {
    SDL_Event e;
    while(SDL_PollEvent(&e)) {
        switch(e.type) {
            case SDL_WINDOWEVENT_CLOSE:
            case SDL_QUIT:
                return false;
            case SDL_KEYDOWN:
            switch(e.key.keysym.sym) {
                case SDLK_q:
                    return false;
            }
            default:
                break;
        }
    }
    glClear(GL_COLOR_BUFFER_BIT);
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, c_Img.i_Width, c_Img.i_Height, GL_RGB, GL_UNSIGNED_BYTE, c_Img.i_Data);
    glUseProgram(c_Prog);
    glBindVertexArray(c_VAO);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    SDL_GL_SwapWindow(c_Win);
    return true;
}

Context::~Context() {
    SDL_Quit();
}
#endif

Texture::Texture() {
    t_Data = nullptr;
    t_Width = 0;
    t_Height = 0;
    t_Fallback = Color(1.0, 1.0, 1.0);
}

Texture::Texture(const char* filename) : t_Fallback(0, 0, 0) {
    t_Data = stbi_load(filename, &t_Width, &t_Height, &t_Comp, 0);
    std::cout << "Loaded texture: " << filename << " (" << t_Width << "x" << t_Height << ")\n";
}

Texture::Texture(Texture &&other){
    t_Data = other.t_Data;
    t_Width = other.t_Width;
    t_Height = other.t_Height;

    other.t_Data = nullptr;
    other.t_Width = 0;
    other.t_Height = 0;
}

void Texture::operator=(Texture&& other){
    if(t_Data){free(t_Data); t_Data=nullptr;}
    t_Data = other.t_Data;
    t_Width = other.t_Width;
    t_Height = other.t_Height;
    t_Comp = other.t_Comp;
    t_Fallback = other.t_Fallback;

    other.t_Data = nullptr;
    other.t_Width = 0;
    other.t_Height = 0;
}

void Texture::operator=(const Texture& other) {
    if(t_Data){free(t_Data); t_Data = nullptr;}
    t_Data = (uint8_t*) malloc(other.t_Width * other.t_Height * 3);
    memcpy(t_Data, other.t_Data, other.t_Width * other.t_Height * 3);
    t_Width = other.t_Width;
    t_Height = other.t_Height;
    t_Comp = other.t_Comp;
    t_Fallback = other.t_Fallback;
}

Texture::~Texture() {
    if(t_Data) free(t_Data);
}

Color Texture::at(int i, int j) const{
    if(i >= t_Width || j >= t_Height) return t_Fallback;
    return Color(
        (float)t_Data[j * t_Width * 3 + i * 3] * 1.0f / 255.0f,
        (float)t_Data[j * t_Width * 3 + i * 3 + 1] * 1.0f / 255.0f,
        (float)t_Data[j * t_Width * 3 + i * 3 + 2] * 1.0f / 255.0f
    );
}
