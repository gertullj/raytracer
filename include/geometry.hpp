#pragma once
#include <general.hpp>

#include <glm/glm.hpp>
#include <glm/gtc/random.hpp>
#include <glm/common.hpp>

#include <draw.hpp>


typedef uint32_t MatID;

struct Sphere {
    glm::vec3 s_Position;
    float s_Radius;
    MatID s_Mat;
};

struct Plane {
    glm::vec3 p_Point;
    glm::vec3 p_Normal;
    MatID p_Mat;
};

struct Triangle {
    glm::vec3 t_V0;
    glm::vec3 t_V1;
    glm::vec3 t_V2;
    glm::vec3 t_N0;
    glm::vec3 t_N1;
    glm::vec3 t_N2;
    MatID t_Mat;
    Triangle(
        const glm::vec3 v0, 
        const glm::vec3 v1, 
        const glm::vec3 v2, 
        const glm::vec3 n0, 
        const glm::vec3 n1, 
        const glm::vec3 n2, 
        const MatID matID) : t_V0(v0), t_V1(v1), t_V2(v2), t_N0(n0), t_N1(n1), t_N2(n2), t_Mat(matID){}
};

struct Ray{
    //Ray(x) = direction * x + origin
    glm::vec3 r_Origin, r_Direction;

    constexpr Ray(const glm::vec3 origin, const glm::vec3 direction) : r_Origin(origin), r_Direction(direction) {};
    
    constexpr auto at(float t) const -> glm::vec3 {return r_Direction * t + r_Origin;}
    
    auto reflect(const glm::vec3 normal) const -> Ray {
        return Ray(r_Origin, glm::reflect(-r_Direction, normal));
    }
};

struct Camera{
    glm::vec3 c_Position;
    glm::vec3 c_Direction;
    glm::vec3 c_Up;
    float c_Width, c_Height;
};

struct Scene{
    std::vector<Sphere> s_Spheres;
    std::vector<Plane> s_Planes;
    std::vector<Triangle> s_Triangles;
    std::vector<Material> s_Materials;
    Texture s_Skybox;
    Camera s_Camera;
    Scene();
};

auto readOBJ(const char* filename, const float scale, const int default_material) -> std::vector<Triangle>;