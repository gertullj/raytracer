#pragma once

#include <cstdlib>
#include <cstdint>
#include <ctime>

#include <vector>
#include <tuple>
#include <limits>
#include <iostream>
#include <iomanip>
#include <random>
#include <chrono>
#include <thread>
#include <algorithm>
#include <atomic>
#include <fstream>
#include <sstream>
#include <string>

#ifdef _WIN32
#undef GUI
#endif

struct RGB888 {
    uint8_t r;
    uint8_t g;
    uint8_t b;
};

struct Color {
    float c_r;
    float c_g;
    float c_b;

    constexpr inline Color() : c_r(0.0), c_g(0.0), c_b(0.0){}

    constexpr inline Color(const float r, const float g, const float b) : c_r(r), c_g(g), c_b(b){}
    
    constexpr inline auto operator+(const Color &rhs) const -> Color {
        return Color(c_r + rhs.c_r, c_g + rhs.c_g, c_b + rhs.c_b);
    }
    
    constexpr inline auto operator*(const Color &rhs) const -> Color {
        return Color(c_r * rhs.c_r, c_g * rhs.c_g, c_b * rhs.c_b);
    }
    
    constexpr inline auto operator*(const float scale) const -> Color {
        return Color(c_r * scale, c_g * scale, c_b * scale);
    }
    
    constexpr inline auto mix(const Color &rhs, float mix) const -> Color {
        return Color(
            std::sqrt((1.0 - mix) * c_r * c_r + mix * rhs.c_r * rhs.c_r), 
            std::sqrt((1.0 - mix) * c_g * c_g + mix * rhs.c_g * rhs.c_g),
            std::sqrt((1.0 - mix) * c_b * c_b + mix * rhs.c_b * rhs.c_b)
        );
    }

    static auto mix(const Color *begin, const Color *end) -> Color;

    constexpr inline auto rgb888() const -> RGB888 {
        return RGB888{.r = std::min((uint8_t)255, (uint8_t)(c_r * 255.0)), .g = std::min((uint8_t)255, (uint8_t)(c_g * 255.0)), .b = std::min((uint8_t)255, (uint8_t)(c_b * 255.0))};
    }
};

constexpr Color Black(0.0f, 0.0f, 0.0f);
constexpr Color White(1.0f, 1.0f, 1.0f);


struct Material{
    Color m_Color;
    float m_Reflect;
    float m_Diffuse;
};

