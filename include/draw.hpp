#pragma once

#include <general.hpp>

#ifdef GUI
#include <SDL2/SDL.h>
#include <glad/glad.h>
#endif

struct Img {
    size_t i_Width = 0;
    size_t i_Height = 0;
    RGB888 *i_Data = nullptr;
    Img() = default;
    Img(const Img& other);
    Img(Img&& other);
    Img(const size_t width, const size_t height);
    void operator=(const Img& other);
    void operator=(Img&& other);
    ~Img();
    RGB888& at(const size_t i, const size_t j);
    void write(std::ostream& stream);
    void savePPM(const std::string& filename);
    void savePPM(const char* filename);
};

struct Texture {
    int t_Width, t_Height, t_Comp;
    unsigned char *t_Data;
    Color t_Fallback;

    Texture();
    Texture(const char* filename);
    Texture(Texture &&other);
    void operator=(Texture&& other);
    void operator=(const Texture& other);
    ~Texture();
    Color at(int i, int j) const;
};

#ifdef GUI
struct Context {
    GLuint c_VAO, c_VBO, c_Tex, c_Prog;
    int c_Width, c_Height;
    SDL_Window *c_Win;
    SDL_GLContext c_GL_ctx;
    Img c_Img;


    Context(const char *title, int width, int height);
    ~Context();
    bool update();
};
#endif


