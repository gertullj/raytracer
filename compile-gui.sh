#!/usr/bin/bash
g++ -std=c++17 src/*.cpp src/glad.c -O3 -ffast-math -Wall -Wextra -Iinclude -lSDL2 -lGL -lGLEW -ldl -o raytracer -lpthread -march=native -DVERSION2 -DGUI $@
